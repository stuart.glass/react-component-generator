import React from 'react'
import { storiesOf } from '@storybook/react'
import REPLACE_COMPONENT_NAME from './REPLACE_COMPONENT_NAME'

storiesOf('REPLACE_COMPONENT_NAME', module).add(
  'REPLACE_COMPONENT_NAME',
  () => <REPLACE_COMPONENT_NAME />
)
