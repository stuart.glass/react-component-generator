import React from 'react'
import { Wrapper } from './REPLACE_COMPONENT_NAME.styled'

const REPLACE_COMPONENT_NAME = (): React.ReactElement => {
  return <Wrapper>REPLACE_COMPONENT_NAME</Wrapper>
}

export default REPLACE_COMPONENT_NAME
