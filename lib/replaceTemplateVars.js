const replace = require('replace-in-file')

const run = async (componentFiles, componentName) => {
  const files = componentFiles.map((component) => component.target)
  const options = {
    files: files,
    from: /REPLACE_COMPONENT_NAME/g,
    to: componentName
  }
  try {
    const results = await replace(options)
  } catch (error) {
    console.error('Error occurred:', error)
  }
}

module.exports = run
